FROM ubuntu:23.04

MAINTAINER Anush

# ---------------------------
# --- Install required tools

ENV DEBIAN_FRONTEND=noninteractive


RUN apt update -y && \
	apt install -y \
		curl \
		git \
		expect \
		wget \
		zip \
		unzip \
		chromium-browser

RUN apt-get clean


RUN apt install -y nodejs

RUN apt install -y npm

RUN node -v 
RUN npm -v

RUN curl -sL https://sentry.io/get-cli/ | bash
RUN sentry-cli --help

RUN npm i -g browserslist caniuse-lite --save